package cn.wps.moffice_eng;

import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.share.Sharer;
import com.facebook.share.model.ShareMessengerActionButton;
import com.facebook.share.model.ShareMessengerGenericTemplateContent;
import com.facebook.share.model.ShareMessengerGenericTemplateElement;
import com.facebook.share.model.ShareMessengerURLActionButton;
import com.facebook.share.widget.MessageDialog;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import moffice.wps.cn.messenger.R;

public class MainActivity extends AppCompatActivity {

    CallbackManager callbackManager = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        callbackManager = CallbackManager.Factory.create();

	    try {
		    PackageInfo info = getPackageManager().getPackageInfo(
			    "cn.wps.moffice_eng",
			    PackageManager.GET_SIGNATURES);
		    for (Signature signature : info.signatures) {
			    MessageDigest md = MessageDigest.getInstance("SHA");
			    md.update(signature.toByteArray());
			    Log.d("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
		    }
	    } catch (PackageManager.NameNotFoundException e) {
			e.printStackTrace();
	    } catch (NoSuchAlgorithmException e) {
		    e.printStackTrace();
	    }

        Button textView = this.findViewById(R.id.aaa);
        textView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ShareMessengerURLActionButton actionButton =
                        new ShareMessengerURLActionButton.Builder()
                                .setTitle("这个是分享链接标题")
                                .setUrl(Uri.parse("https://docs.wps.com/#/doc"))
                                .build();
                ShareMessengerActionButton defaultbtn =
                        new ShareMessengerURLActionButton.Builder()
                        .setTitle("这个是默认标题？").build();
                ShareMessengerGenericTemplateElement genericTemplateElement =
                        new ShareMessengerGenericTemplateElement.Builder()
                                .setTitle("模板标题")
                                .setSubtitle("模板次标题")
                                .setImageUrl(Uri.parse("https://ss1.baidu.com/6ONXsjip0QIZ8tyhnq/it/u=1481692567,703063668&fm=173&app=25&f=JPEG?w=600&h=288&s=CD7409C2316785070FD4F818030000D1"))
                                .setButton(actionButton)
                                .setDefaultAction(defaultbtn)
                                .build();
                ShareMessengerGenericTemplateContent genericTemplateContent =
                        new ShareMessengerGenericTemplateContent.Builder()
                                .setPageId("745276635805499") // Your page ID, required 745276635805499
                                .setGenericTemplateElement(genericTemplateElement)
                                .build();

                if (MessageDialog.canShow(ShareMessengerGenericTemplateContent.class)) {
                    MessageDialog messageDialog = new MessageDialog(MainActivity.this);
                    messageDialog.registerCallback(callbackManager, new FacebookCallback<Sharer.Result>() {
                        @Override
                        public void onSuccess(Sharer.Result result) {
                            Log.e("ssss", result.getPostId() + "");
                        }

                        @Override
                        public void onCancel() {
                            Log.e("ssss", "cacecl");
                        }

                        @Override
                        public void onError(FacebookException error) {
                            Log.e("ssss", error.toString());
                        }
                    });
                    messageDialog.show(genericTemplateContent);
                }
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }
}
